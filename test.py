from msv import conntest
import pytest


def test_connectable_host():
    assert(conntest.tcp_connection_test("google.com", 80) == False)

def test_non_connectable_host():
    assert(conntest.tcp_connection_test("google.com", 81) == True)
