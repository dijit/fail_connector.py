def config_generator():
    """Returns a dict with the contents of the config file
    """
    import yaml as _yaml
    import argparse as _argparse
    _parser = _argparse.ArgumentParser()
    _parser.add_argument("-c", "--config",
                         help="Config file to read from for connection.",
                         type=str, default="./connector.conf")
    _parser.add_argument("-v", "--verbose",
                         help="Verbosity, runs silently unless specified.",
                         action="store_true")
    _args = _parser.parse_args()
    del _parser, _argparse

    try:
        with open(_args.config, 'r') as conf:
            return _yaml.load(conf)
    except:
        print("No config file found")
        raise FileNotFoundError
