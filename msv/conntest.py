import socket

def tcp_connection_test(host, port):
    """Creates a connection to host:port
    returns true if timeout or resource unavailable
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(2)
    result = sock.connect_ex((str(host), int(port)))
    # http://www.jbox.dk/sanos/source/include/errno.h.html
    # 70 == ETIMEDOUT,
    # 11 == EDEADLK (An attempt was made to lock a system resource that would have resulted in a deadlock situation.)
    # 35 == EAGAIN (Resource Temporarily Unavailable)
    print(result)
    if result == 70 or result == 11 or result == 35:
        return True
    else:
        return False
