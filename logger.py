#!/usr/bin/env python
"""
This Script supplements run.py
This script exists to discover _which_ host is accessible.
"""
from time import sleep

from msv import conntest
from msv import config

if __name__ == '__main__':
    CONFIG = config.config_generator()
    for value in CONFIG['checkhosts'].values():
        if not conntest.tcp_connection_test(value['host'], value['port']):
            print("{}:{} is open".format(value['host'],value['port']))
        else:
            print("{}:{} is closed".format(value['host'],value['port']))
