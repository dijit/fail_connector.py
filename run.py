#!/usr/bin/env python
"""
This Script expects to never be able to connect to anything.
Success is failure, and will be reported as such.

input: YAML config file: |
---
zabbix:
    host: "localhost"
    port: 10051
checkhosts:
    prettyname:
        - host: "google.com"
        - port: 81
    prettyname2:
        - host: <string>
        - port: <int>
"""
from time import sleep
from socket import gethostname

from msv import zabbix
from msv import conntest
from msv import config


def zabsend(statusno):
    zabbix.send_to_zabbix(zabbix_host=CONFIG['zabbix']['host'],
                          zabbix_port=CONFIG['zabbix']['port'],
                          metrics=[zabbix.Metric(gethostname(), "FirewallOpen",
                                                 int(statusno))])

if __name__ == '__main__':
    CONFIG = config.config_generator()
    while 1:
        _accumulator = []
        for value in CONFIG['checkhosts'].values():
            if conntest.tcp_connection_test(value['host'], value['port']):
                _accumulator.append("{}:{}".format(value['host'],value['port']))

        print(_accumulator)
        if _accumulator:
            zabsend(1)
        else:
            zabsend(0)

        sleep(60)
